package archive.treeArchive;

/**
 * Created by Users on 04.05.2017.
 */
public class Node implements Comparable<Node>{
    private String key;
    private int frequency;
    private Node left, right, parent;

    public Node(String key, int frequency) {
        this.key = key;
        this.frequency = frequency;
    }


    public String getKey(){
        return key;
    }
    public int getFrequency() {
        return frequency;
    }
    public Node getLeft() {
        return left;
    }
    public Node getRight() {
        return right;
    }
    public Node getParent() {
        return parent;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
    public void setLeft(Node left) {
        this.left = left;
    }
    public void setRight(Node right) {
        this.right = right;
    }
    public void setParent(Node parent) {
        this.parent = parent;
    }

    public boolean hasLeftChild(){
        return left != null;
    }
    public boolean hasRightChild(){
        return right != null;
    }
    public boolean isLeaf() {
        return (left == null && right == null) ? true : false;
    }

    @Override
    public int compareTo(Node node) {
        return this.getFrequency() - node.getFrequency();
    }
}
