package archive.treeArchive;

import java.util.*;

/**
 * Created by Users on 04.05.2017.
 */
public class TreeArchive {
    private Node root;

    public Node getRoot() {
        return root;
    }
    // очередь из узлов, на основе которой строится дерево
    private PriorityQueue<Node> nodesPriorityQueue = new PriorityQueue<>();

    public TreeArchive(Map<String, Integer> mapOfFrequencies) {
        List<Node> nodesArrayList = new ArrayList<>();
        // заполнение
        mapOfFrequencies.forEach((key, frequency) -> nodesArrayList.add(new Node(key, frequency)));
        nodesArrayList.sort(Comparator.naturalOrder());

        nodesArrayList.forEach(node -> nodesPriorityQueue.add(node));
    }

    // построение дерева на основе таблицы частот
    public void buildTree() {
        while(nodesPriorityQueue.size() != 1) {  // оставшийся элемент в очереди - корень дерева
            // объединяемые узлы
            Node node1 = nodesPriorityQueue.poll();
            Node node2 = nodesPriorityQueue.poll();
            // родитель для двух данных без ключа и частоты
            Node parent = new Node(null, 0);
            parent.setFrequency(node1.getFrequency() + node2.getFrequency());
            // связка
            node1.setParent(parent);
            node2.setParent(parent);
            parent.setLeft(node1);
            parent.setRight(node2);
            // добавление в очередь нового узла
            nodesPriorityQueue.add(parent);
        }
        // корень дерева, построенного на основе анализа частот
        root = nodesPriorityQueue.poll();
    }

    // получение кода символа в виде строки
    private String getCodeOfCharacter(Node currentNode, String currentKey) {
        if (currentNode != null) {
            if (currentNode.hasLeftChild()) getCodeOfCharacter(currentNode.getLeft(), currentKey += "0");
            else return currentKey;
            if (currentNode.hasRightChild()) getCodeOfCharacter(currentNode.getRight(), currentKey += "1");
            else return currentKey;
        } return currentKey;
    }
    // после построения дерева строится таблица Хаффмана. Данная таблица —
    // это по сути связный список или массив, который содержит каждый символ
    // и его код, потому что это делает кодирование более эффективным

}
