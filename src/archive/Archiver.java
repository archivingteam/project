package archive;

import archive.treeArchive.Node;
import archive.treeArchive.TreeArchive;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;

/**
 * Created by Users on 04.05.2017.
 */
public class Archiver {
    private static final String TABLE_SEPARATOR = "☼";
    private static final String TABLE_SEPARATOR_REGEX = "([☼]){1}";
    private static final String EXTENSION = ".arc";
    private static final String DEFAULT_EXTENSION = "default";
    private static final String DEFAULT_EXTENSION_TXT = ".txt";
    private static final String DIRECTORY_TO_DECOMPRESS = "output";
    private static final String MARK_COMPRESSED = "Compressed";
    private static final String MARK_DECOMPRESSED = "Decompressed";
    private static final String MARK_COMPRESS_TABLE = "CompressTable";

    private static Map<String, String> archiveTable; // таблица архивации
    private static TreeArchive archiveTree; // дерево архивации

//------------------------------------------------Общие методы--------------------------------------------------------//
    // запись текста в файл с возвращением файла
    private static void writeTextToFile(String text, File file) throws IOException {
        FileChannel fileChannel = new FileOutputStream(file).getChannel();
        ByteBuffer buffer = ByteBuffer.wrap(text.getBytes());
        fileChannel.write(buffer);
        fileChannel.close();    buffer.clear();
    }
    // чтение текста из файла с возвращением текста файла
    private static String readTextFromFile(File input) throws IOException {
        FileChannel fileChannel = new FileInputStream(input).getChannel();
        ByteBuffer buffer = ByteBuffer.allocate((int) (input.length()));

        String text = "";
        while (buffer.hasRemaining()) {
            fileChannel.read(buffer);
            text += new String(buffer.array());
        }
        fileChannel.close();    buffer.clear();
        return text;
    }
    // получение имени нового файла с заданным расширением
    private static String getNewFilePath(File input, String mark, String extension) {
        String decompressedFileName = input.getName();  // имя нового файла составляется с основой имени старого
        int indexOfExtension = decompressedFileName.indexOf('.');   // индекс разделения
        if (indexOfExtension != -1) {   // если input - файл
            // разибение файла на имя и расширение
            String[] fileParts = splitOnNameAndExtension(indexOfExtension, decompressedFileName);
            fileParts[0] += mark; // добаление пометки к имени файла
            decompressedFileName = fileParts[0];    // имя нового файла
            if (!extension.equals(DEFAULT_EXTENSION)) decompressedFileName += extension;    // добавление расширения к новому имени
            else decompressedFileName += "." + fileParts[1];  // оставить расширение по умолчанию
        } else decompressedFileName += mark;    // иначе просто приписать пометку
        return input.getParent() + "/" + decompressedFileName;    // полный путь к новому файлу
    }
    // получение имени нового файла с заданным расширением в новую директорию
    private static String getNewFilePath(File input, String mark, String extension, File compressedDirectory) {
        String decompressedFileName = input.getName();
        int indexOfExtension = decompressedFileName.indexOf('.');   // индекс разделения
        if (indexOfExtension != -1) {
            // разибение файла на имя и расширение
            String[] fileParts = splitOnNameAndExtension(indexOfExtension, decompressedFileName);
            fileParts[0] += mark; // добаление пометки к имени файла
            decompressedFileName = fileParts[0];    // имя нового файла
            if (!extension.equals(DEFAULT_EXTENSION)) decompressedFileName += extension;    // добавление расширения к новому имени
            else decompressedFileName += "." + fileParts[1];  // оставить расширение по умолчанию
        }
        return compressedDirectory.getPath() + "/" + decompressedFileName;    // полный путь к новому файлу
    }
    // папка для деархивации
    private static File getDirectoryToDecompress(File input, String directoryName) {
        String directoryToDecompressPath = input.getParent(); // путь до каталога с файлом
        File directoryToDecompressFile;
        if (!directoryName.contains(input.getParent())) {
            directoryToDecompressFile = new File(directoryToDecompressPath + "/" + directoryName);
        } else {
            directoryToDecompressFile = new File(directoryName);
        }
        directoryToDecompressFile.mkdir();
        return directoryToDecompressFile;    // полный путь к новой папке
    }
    // разделение полного имени на имя файла и рсширение
    private static String[] splitOnNameAndExtension(int indexOfExtension, String decompressedFileName) {
        String[] fileParts = new String[2]; // две части: имя, расширение
        fileParts[0] = decompressedFileName.substring(0, indexOfExtension);
        fileParts[1] = decompressedFileName.substring(indexOfExtension +  1, decompressedFileName.length());
        return fileParts;
    }

//------------------------------------------------Архивация-----------------------------------------------------------//
    // архивация
    public static void compress(File input) {
        try {
            if (input.isDirectory()) compressDirectory(input);
            else compressFile(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // архивиция текста
    private static String compressText(File input) throws IOException {
        String inputText = readTextFromFile(input);
        Map<String, Integer> lettersFrequency = createMapOfFrequencies(inputText);
        archiveTree = new TreeArchive(lettersFrequency);
        archiveTree.buildTree();
        archiveTable = new HashMap<>();
        createArchiveTable(archiveTree.getRoot(), "");
        String bitsString = toBitsString(inputText, archiveTable);
        return bitsToString(bitsString);
    }
    // архивиция файла
    private static void compressFile(File input) throws IOException {
        String archivedText = compressText(input);
        decompressTable(new File(getNewFilePath(input, MARK_COMPRESS_TABLE, DEFAULT_EXTENSION)));
        writeTextToFile(archivedText, new File(getNewFilePath(input, MARK_COMPRESSED, EXTENSION)));
    }
    // архивация файла в новую директорию
    private static void compressFileInNewDirectory(File input, File compressedDirectory) throws IOException {
        String archivedText = compressText(input);
        decompressTable(new File(getNewFilePath(input, MARK_COMPRESS_TABLE, DEFAULT_EXTENSION, compressedDirectory)));
        writeTextToFile(archivedText, new File(getNewFilePath(input, MARK_COMPRESSED, EXTENSION, compressedDirectory)));
    }

    // архивиция папки
    private static void compressDirectory(File input) throws IOException {
        File compressedDirectory = new File(getNewFilePath(input, MARK_COMPRESSED, DEFAULT_EXTENSION));
        compressedDirectory.mkdir();
        File[] files = input.listFiles();   // все файлы директории
        for (File file : files) {
            compressInNewDirectory(file, compressedDirectory);
        }
    }
    // архивация в новую директорию
    private static void compressInNewDirectory(File input, File compressedDirectory) throws IOException {
        // TODO compressDirectoryInNewDirectory
        // if (input.isDirectory()) compressDirectoryInNewDirectory(input, compressedDirectory);
        compressFileInNewDirectory(input, compressedDirectory);
    }

    // запись таблицы архивации в файл
    private static void decompressTable(File decompressTableFile) throws IOException {
        writeTextToFile(getArchiveTableAsString(), decompressTableFile);
    }

//---------------------------------------------Вспомогательные методы-------------------------------------------------//
    // составление таблицы частот для символов их текста
    private static Map<String, Integer> createMapOfFrequencies(String text) {
        // Character = символ Integer = его частота
        Map<String,Integer> lettersFrequency = new HashMap<>();
        for (int i = 0; i < text.length(); i++){
            if (!lettersFrequency.keySet().contains(text.charAt(i)))
                lettersFrequency.put(String.valueOf(text.charAt(i)), 1);
            else
                lettersFrequency.put(String.valueOf(text.charAt(i)), lettersFrequency.get(text.charAt(i)) + 1);
        }

        lettersFrequency.entrySet().stream()
                .sorted(Comparator.comparingInt(Map.Entry::getValue));
        return lettersFrequency;
    }
    // построение таблицы архивирования
    private static void createArchiveTable(Node currentNode, String key) {
        if (currentNode != null) {
            if (currentNode.isLeaf()) archiveTable.put(currentNode.getKey(), key);
            else {
                if (currentNode.hasLeftChild()) createArchiveTable(currentNode.getLeft(), key + "0");
                if (currentNode.hasRightChild()) createArchiveTable(currentNode.getRight(), key + "1");
            }
        }
    }
    // архивация текста
    private static String toBitsString(String inputText, Map<String, String> archiveTable) {
        String archivedText = "";   // заархивированная строка
        for (int i = 0; i < inputText.length(); i++)
            archivedText += archiveTable.get(String.valueOf(inputText.charAt(i)));
        return archivedText;
    }
    // перевод строки из битов в слова
    private static String bitsToString(String bitsString) {
        String archivedString = ""; // заархивирвоанная строка
        int processedPart = 0;  // индекс
        int stringLength = bitsString.length();
        String oneByte; // представление в виде строки последовательность битов
        while (processedPart < stringLength) {  // перевод последовательности бит в строку
            // выделение байта
            if (stringLength - processedPart >= 8)
                oneByte = bitsString.substring(processedPart, processedPart += 8);
            else
                oneByte = bitsString.substring(processedPart, processedPart += stringLength - processedPart);
            // добавление байта в виде символа к строке
            archivedString += ((char) Integer.parseInt(oneByte, 2));
        }
        return archivedString;
    }
    // представление таблицы архивации в виде строки
    private static String getArchiveTableAsString() {
        String archiveTableString = "";
        Set<String> keySet = archiveTable.keySet();
        String entry;  // будущая запись в таблице
        for (String key : keySet) {
            entry = "";
            switch (key) {
                case "\n":
                    entry += "\\n";
                    break;
                case "\r":
                    entry += "\\r";
                    break;
                default:
                    entry += key;
                    break;
            }
            archiveTableString += entry + TABLE_SEPARATOR + archiveTable.get(key) + "\n";
        }
        return archiveTableString;
    }

//------------------------------------------------Деархивация---------------------------------------------------------//
    // TODO подогнать под задание. Деархивация любого объекта в ПАПКУ. Подключение таблицы в методы, а не через параметр
    // деархивация в новую папку
    public static void decompress(File input) throws Exception {
        if (input.isDirectory())
            decompressDirectory(input, getDirectoryToDecompress(input, DIRECTORY_TO_DECOMPRESS));
        else {
            if (("." + splitOnNameAndExtension(input.getName().indexOf('.'), input.getName())[1]).equals(EXTENSION)) {
                try {
                    decompressFile(input, getDirectoryToDecompress(input, DIRECTORY_TO_DECOMPRESS));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else throw new Exception("Wrong format!");
        }
    }
    // деархивация папки
    private static void decompressDirectory(File input, File newDirectory) throws IOException {
        File[] files = input.listFiles();   // все файлы директории
        // новая директория должна лежать в output
        File decompressedDirectory =
                getDirectoryToDecompress(input, getNewFilePath(input, MARK_DECOMPRESSED, DEFAULT_EXTENSION));
        decompressedDirectory.mkdir();
        for (File file : files) {
            decompressFile(file, decompressedDirectory);
        }
    }
    // деархивация файла
    private static void decompressFile(File input, File newDirectory) throws IOException {
        if (input.getName().contains(EXTENSION)) {
            String inputText = readTextFromFile(input);
            String stringOfBits = stringToBits(inputText);
            Map<String, String> decompressTableMap = initializeDecompressTable(findDecompressTableMap(input));
            String decompressedText = decompressText(decompressTableMap, stringOfBits);
            writeTextToFile(decompressedText, new File(getNewFilePath(input, MARK_DECOMPRESSED, DEFAULT_EXTENSION, newDirectory)));
        }
    }

    //---------------------------------------------Вспомогательные методы-------------------------------------------------//
    // инициализация таблицы архивации
    private static Map<String, String> initializeDecompressTable(File decompressTableFile) throws IOException {
        Map<String, String> decompressTableMap = new HashMap<>();    // значение, ключ! -> код, символ
        String dataForBuildMap = readTextFromFile(decompressTableFile);
        fillMap(decompressTableMap, dataForBuildMap);
        return decompressTableMap;
    }
    // поиск таблицы архивации для input. Таблица сохраняется в том же каталоге с схожим названием что и input + CompressTable
    private static File findDecompressTableMap(File input) {
        File inputDirectory = input.getParentFile();    // директория в которой находится input
        String decompressTableMapName =
                input.getName().replace(MARK_COMPRESSED + EXTENSION, "") +
                        MARK_COMPRESS_TABLE + DEFAULT_EXTENSION_TXT;
        File[] files = inputDirectory.listFiles();
        // поиск таблицы архивации в каталоге
        return Arrays.stream(files).filter(file -> file.getName().equals(decompressTableMapName)).findFirst().get();
    }
    // заполенение таблицы архивации данными из файла (перевернута, для удобного получения ключа по значению (decompressText))
    private static void fillMap(Map<String, String> decompressTableMap, String dataForBuildMap) {
        String[] entries = dataForBuildMap.split("\n");
        for (String entry : entries) {
            String[] data = entry.split(TABLE_SEPARATOR_REGEX);
            decompressTableMap.put(data[1], data[0]);
        }
    }
    // перевод двоичной строки в разархивированный текст
    private static String decompressText(Map<String, String> decompressTableMap, String stringOfBits) {
        String decompressedText = "";   // разархивированный текст
        String currentSequenceOfBit = "";    // текущая последовательность битов
        String currentCharacter;    // текущий символ
        for (int i = 0; i < stringOfBits.length(); i++) {
            currentSequenceOfBit += String.valueOf(stringOfBits.charAt(i));
            // если данная последовательность нулей и единиц есть в таблице архивации
            if (decompressTableMap.containsKey(currentSequenceOfBit)) {
                currentCharacter = String.valueOf(decompressTableMap.get(currentSequenceOfBit));
                if (currentCharacter.equals("\\n")) decompressedText += "\n";
                else if (currentCharacter.equals("\\r")) decompressedText += "\r";
                else decompressedText += currentCharacter;
                currentSequenceOfBit = "";  // сброс текущей последовательности (все значения таблицы УНИКАЛЬНЫ!)
            }
        }
        return decompressedText;
    }
    // перевод строки символов в двоичный вид
    private static String stringToBits(String inputText) {
        String stringOfBits = "";   // строка из битов
        for (int i = 0; i < inputText.length(); i++) {  // перевод текста в двоичный вид
            int b = inputText.charAt(i);
            String s = Integer.toString(b, 2);
            // у полседнего байта нет незначащих нулей
            if (s.length() < 8 && i != inputText.length() - 1) s = String.format("%8s", s).replaceAll(" ", "0");
            stringOfBits += s;
        }
        return stringOfBits;
    }
}
